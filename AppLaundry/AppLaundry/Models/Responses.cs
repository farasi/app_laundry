﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppLaundry.Models
{
    public class Responses
    {
    }

    public  class ResponsesKaryawan
    {
        public  string Message { get; set; }
        public  bool Status { get; set; }
        public  view_karyawan Data { get; set; }
    }

    public class ResponsesPelanggan
    {
        public string Message { get; set; }
        public bool Status { get; set; }
        public view_pelanggan Data { get; set; }
    }



    public class ResponsesDaftarHarga
    {
        public string Message { get; set; }
        public bool Status { get; set; }
        public view_daftar_harga Data { get; set; }
    }

}