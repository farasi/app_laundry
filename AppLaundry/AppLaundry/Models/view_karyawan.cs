//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AppLaundry.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class view_karyawan
    {
        public System.Guid karyawan_id { get; set; }
        public string karyawan_nama { get; set; }
        public string karyawan_alamat { get; set; }
        public string karyawan_phone { get; set; }
        public string karyawan_username { get; set; }
        public string karyawan_password { get; set; }
        public Nullable<System.DateTime> karyawan_created_datetime { get; set; }
        public Nullable<System.DateTime> karyawan_modified_datetime { get; set; }
        public Nullable<bool> karyawan_deleted { get; set; }
    }
}
