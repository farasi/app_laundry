//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AppLaundry.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_detail_transaksi
    {
        public System.Guid detail_transaksi_id { get; set; }
        public Nullable<System.Guid> detail_transaksi_transaksi_id { get; set; }
        public Nullable<System.Guid> detail_transaksi_daftar_harga_id { get; set; }
        public string detail_transaksi_keterangan { get; set; }
        public Nullable<int> detail_transaksi_kuantitas { get; set; }
        public Nullable<int> detail_transaksi_harga_satuan { get; set; }
        public Nullable<int> detail_transaksi_harga_subtotal { get; set; }
        public string detail_transaksi_no_invoice { get; set; }
        public string detail_transaksi_status { get; set; }
    
        public virtual tbl_daftar_harga tbl_daftar_harga { get; set; }
        public virtual tbl_transaksi tbl_transaksi { get; set; }
    }
}
