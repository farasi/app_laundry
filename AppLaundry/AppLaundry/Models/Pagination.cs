﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppLaundry.Models
{
    public class Pagination
    {

    }


    public class PaginationKaryawan
    {
        public int draw { get; set; }
        public int recordsFiltered { get; set; }
        public int recordsTotal { get; set; }
        public List<view_karyawan> data { get; set; }
    }


    public class PaginationPelanggan
    {
        public int draw { get; set; }
        public int recordsFiltered { get; set; }
        public int recordsTotal { get; set; }
        public List<view_pelanggan> data { get; set; }
    }



    public class PaginationDaftarHarga
    {
        public int draw { get; set; }
        public int recordsFiltered { get; set; }
        public int recordsTotal { get; set; }
        public List<view_daftar_harga> data { get; set; }
    }


    public class PaginationTransaksi
    {
        public int draw { get; set; }
        public int recordsFiltered { get; set; }
        public int recordsTotal { get; set; }
        public List<view_transaksi> data { get; set; }
    }



    public class PaginationTransaksiDetail
    {
        public int draw { get; set; }
        public int recordsFiltered { get; set; }
        public int recordsTotal { get; set; }
        public List<view_transaksi_detail> data { get; set; }
    }


}