//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AppLaundry.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_tipe
    {
        public System.Guid tipe_id { get; set; }
        public Nullable<System.Guid> tipe_daftar_harga_id { get; set; }
        public Nullable<System.DateTime> tipe_created_datetime { get; set; }
        public Nullable<System.DateTime> tipe_modified_datetime { get; set; }
        public Nullable<bool> tipe_deleted { get; set; }
    }
}
