﻿using AppLaundry.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AppLaundry.Controllers
{
    public class PelangganController : Controller
    {

        LaundryEntities DB = new LaundryEntities();

        // GET: Karyawan
        public ActionResult Index()
        {
            return View();
        }


        public JsonResult ListPelanggan()
        {

            PaginationPelanggan _pgPelanggan= new PaginationPelanggan();

            try
            {
                var draw = Request.Form.GetValues("draw").FirstOrDefault();
                var start = Request.Form.GetValues("start").FirstOrDefault();
                var length = Request.Form.GetValues("length").FirstOrDefault();
                var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();
                var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();


                // paging
                //Paging Size (10,20,50,100)    
                int pageSize = length != null ? Convert.ToInt32(length) : 0;

                int skip = start != null ? Convert.ToInt32(start) : 0;

                int recordsTotal = 0;


                // Getting all Customer data    
                var _pelanggan = DB.view_pelanggan.ToList();


                //Sorting    
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortDir)))
                {
                    //_karyawan = _karyawan.OrderBy(sortColumn + " " + sortDir);
                }

                //Search    
                if (!string.IsNullOrEmpty(searchValue))
                {
                    //_karyawan = _karyawan.Where(m => m.karyawan_alamat == searchValue);
                }


                //total number of rows count     
                recordsTotal = _pelanggan.Count();

                var data = _pelanggan.Skip(skip).Take(pageSize).ToList();

                _pgPelanggan.draw = Convert.ToInt32(draw);
                _pgPelanggan.recordsFiltered = recordsTotal;
                _pgPelanggan.recordsTotal = recordsTotal;
                _pgPelanggan.data = data;

            }
            catch (Exception ex)
            {

                string message = ex.Message;
            }


            return Json(_pgPelanggan, JsonRequestBehavior.AllowGet);
        }



        public JsonResult UpdatePelanggan(FormCollection form)
        {
            ResponsesPelanggan _rsPelanggan= new ResponsesPelanggan();

            try
            {

                tbl_pelanggan _tbl_pelanggan = new tbl_pelanggan()
                {
                    pelanggan_id = Guid.Parse(form["pelanggan_id"].ToString()),
                    pelanggan_nama = form["pelanggan_nama"].ToString(),
                    pelanggan_alamat = form["pelanggan_alamat"].ToString(),
                    pelanggan_phone = form["pelanggan_phone"].ToString(),

                };


                DB.Entry(_tbl_pelanggan).State = EntityState.Modified;

                int result = DB.SaveChanges();

                if (result > 0)
                {

                    _rsPelanggan.Message = "Berhasil mengubah data";
                    _rsPelanggan.Status = false;
                    _rsPelanggan.Data = DB.view_pelanggan.Where(x => x.pelanggan_id == _tbl_pelanggan.pelanggan_id).SingleOrDefault();

                }
            }
            catch (Exception ex)
            {

                _rsPelanggan.Message = ex.Message;
                _rsPelanggan.Status = false;
                _rsPelanggan.Data = null;
            }


            return Json(_rsPelanggan, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult InsertPelanggan(FormCollection form)
        {

            ResponsesPelanggan _rsPelanggan = new ResponsesPelanggan();


            try
            {
                tbl_pelanggan _tbl_pelanggan = new tbl_pelanggan()
                {
                    pelanggan_id = Guid.NewGuid(),
                    pelanggan_nama = form["pelanggan_nama"].ToString(),
                    pelanggan_alamat = form["pelanggan_alamat"].ToString(),
                    pelanggan_phone = form["pelanggan_phone"].ToString(),
                };

                DB.tbl_pelanggan.Add(_tbl_pelanggan);
                DB.SaveChanges();

                _rsPelanggan.Message = "Berhasil menyimpan data";
                _rsPelanggan.Status = true;
                _rsPelanggan.Data = DB.view_pelanggan.Where(x => x.pelanggan_id == _tbl_pelanggan.pelanggan_id).SingleOrDefault();

            }
            catch (Exception ex)
            {
                _rsPelanggan.Message = ex.Message;
                _rsPelanggan.Status = false;
                _rsPelanggan.Data = null;

            }

            return Json(_rsPelanggan, JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetPelangganById(FormCollection form)
        {
            ResponsesPelanggan _rsPelanggan = new ResponsesPelanggan();



            try
            {
                Guid pelanggan_id = Guid.Parse(form["pelanggan_id"].ToString());

                int rowCount = DB.view_pelanggan.Where(x => x.pelanggan_id == pelanggan_id).Count();

                if (rowCount > 0)
                {
                    _rsPelanggan.Message = "Data ditemukan";
                    _rsPelanggan.Status = true;
                    _rsPelanggan.Data = DB.view_pelanggan.Where(x => x.pelanggan_id == pelanggan_id).SingleOrDefault();

                }


            }
            catch (Exception ex)
            {

                _rsPelanggan.Message = ex.Message;
                _rsPelanggan.Status = false;
                _rsPelanggan.Data = null;
            }

            return Json(_rsPelanggan, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult DeletePelanggan(FormCollection form)
        {
            ResponsesPelanggan _rsPelanggan = new ResponsesPelanggan();
            try
            {
                Guid pelanggan_id = Guid.Parse(form["pelanggan_id"].ToString());

                tbl_pelanggan _tbl_pelanggan = DB.tbl_pelanggan.Find(pelanggan_id);

                if (_tbl_pelanggan != null)
                {
                    DB.tbl_pelanggan.Remove(_tbl_pelanggan);
                    int result = DB.SaveChanges();

                    if (result > 0)
                    {
                        _rsPelanggan.Message = "Berhasil menghapus data.";
                        _rsPelanggan.Status = true;
                        _rsPelanggan.Data = null;
                    }
                    else
                    {
                        _rsPelanggan.Message = "Gagal menghapus data.";
                        _rsPelanggan.Status = false;
                        _rsPelanggan.Data = null;
                    }
                }
            }
            catch (Exception ex)
            {

                _rsPelanggan.Message = ex.Message;
                _rsPelanggan.Status = false;
                _rsPelanggan.Data = null;
            }

            return Json(_rsPelanggan, JsonRequestBehavior.AllowGet);
        }



        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                DB.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}