﻿using AppLaundry.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AppLaundry.Controllers
{
    public class TransaksiController : Controller
    {

        LaundryEntities DB = new LaundryEntities();

        // GET: Transaksi
        public ActionResult Index()
        {
            return View();
        }


        public ActionResult CucianMasuk()
        {
            return View();
        }


        public JsonResult ListCucianMasuk()
        {

            PaginationTransaksi _pgTransaksi = new PaginationTransaksi();
            List<view_transaksi> _viewTransaksi = new List<view_transaksi>();
            try
            {
                var draw = Request.Form.GetValues("draw").FirstOrDefault();
                var start = Request.Form.GetValues("start").FirstOrDefault();
                var length = Request.Form.GetValues("length").FirstOrDefault();
                var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();
                var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();


                // paging
                //Paging Size (10,20,50,100)    
                int pageSize = length != null ? Convert.ToInt32(length) : 0;

                int skip = start != null ? Convert.ToInt32(start) : 0;

                int recordsTotal = DB.view_transaksi.Count();

                int recordsFiltered = 0;


                // Getting all Customer data    
                //Search    
                if (!string.IsNullOrEmpty(searchValue))
                {
                    _viewTransaksi = DB.view_transaksi.Where(m => m.transaksi_status.Contains(searchValue)).ToList();
                }else
                {
                    _viewTransaksi = DB.view_transaksi.ToList();
                }

                //Sorting    
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortDir)))
                {
                    //_karyawan = _karyawan.OrderBy(sortColumn + " " + sortDir);
                }




                //total number of rows count     
                recordsFiltered = _viewTransaksi.Count();

                var data = _viewTransaksi.Skip(skip).Take(pageSize).ToList();

                _pgTransaksi.draw = Convert.ToInt32(draw);
                _pgTransaksi.recordsFiltered = recordsFiltered;
                _pgTransaksi.recordsTotal = recordsTotal;
                _pgTransaksi.data = data;

            }
            catch (Exception ex)
            {

                string message = ex.Message;
            }


            return Json(_pgTransaksi, JsonRequestBehavior.AllowGet);
        }



        public ActionResult CucianMasukBaru()
        {
            return View();
        }


        public ActionResult CucianKeluar()
        {
            return View();
        }



        public ActionResult KlaimCucian()
        {
            return View();
        }


    }
}