﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AppLaundry.Models;

namespace AppLaundry.Controllers
{
    public class TipeController : Controller
    {
        private LaundryEntities db = new LaundryEntities();

        // GET: Tipe
        public ActionResult Index()
        {
            return View(db.tbl_tipe.ToList());
        }

        // GET: Tipe/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_tipe tbl_tipe = db.tbl_tipe.Find(id);
            if (tbl_tipe == null)
            {
                return HttpNotFound();
            }
            return View(tbl_tipe);
        }

        // GET: Tipe/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Tipe/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "tipe_id,tipe_daftar_harga_id,tipe_created_datetime,tipe_modified_datetime,tipe_deleted")] tbl_tipe tbl_tipe)
        {
            if (ModelState.IsValid)
            {
                tbl_tipe.tipe_id = Guid.NewGuid();
                db.tbl_tipe.Add(tbl_tipe);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tbl_tipe);
        }

        // GET: Tipe/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_tipe tbl_tipe = db.tbl_tipe.Find(id);
            if (tbl_tipe == null)
            {
                return HttpNotFound();
            }
            return View(tbl_tipe);
        }

        // POST: Tipe/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "tipe_id,tipe_daftar_harga_id,tipe_created_datetime,tipe_modified_datetime,tipe_deleted")] tbl_tipe tbl_tipe)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tbl_tipe).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tbl_tipe);
        }

        // GET: Tipe/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_tipe tbl_tipe = db.tbl_tipe.Find(id);
            if (tbl_tipe == null)
            {
                return HttpNotFound();
            }
            return View(tbl_tipe);
        }

        // POST: Tipe/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            tbl_tipe tbl_tipe = db.tbl_tipe.Find(id);
            db.tbl_tipe.Remove(tbl_tipe);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
