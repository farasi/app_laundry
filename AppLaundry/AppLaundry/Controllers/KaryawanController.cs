﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AppLaundry.Models;
using System.Data.Entity;

namespace AppLaundry.Controllers
{
    public class KaryawanController : Controller
    {

        LaundryEntities DB = new LaundryEntities();

        // GET: Karyawan
        public ActionResult Index()
        {
            return View();
        }


        public JsonResult ListKaryawan()
        {

            PaginationKaryawan _pgKaryawan = new PaginationKaryawan();

            try
            {
                var draw = Request.Form.GetValues("draw").FirstOrDefault();
                var start = Request.Form.GetValues("start").FirstOrDefault();
                var length = Request.Form.GetValues("length").FirstOrDefault();
                var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();
                var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();


                // paging
                //Paging Size (10,20,50,100)    
                int pageSize = length != null ? Convert.ToInt32(length) : 0;

                int skip = start != null ? Convert.ToInt32(start) : 0;

                int recordsTotal = 0;


                // Getting all Customer data    
                var _karyawan = DB.view_karyawan.ToList();


                //Sorting    
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortDir)))
                {
                    //_karyawan = _karyawan.OrderBy(sortColumn + " " + sortDir);
                }

                //Search    
                if (!string.IsNullOrEmpty(searchValue))
                {
                    //_karyawan = _karyawan.Where(m => m.karyawan_alamat == searchValue);
                }


                //total number of rows count     
                recordsTotal = _karyawan.Count();

                var data = _karyawan.Skip(skip).Take(pageSize).ToList();

                _pgKaryawan.draw = Convert.ToInt32(draw);
                _pgKaryawan.recordsFiltered = recordsTotal;
                _pgKaryawan.recordsTotal = recordsTotal;
                _pgKaryawan.data = data;

            }
            catch (Exception ex)
            {

                string message = ex.Message;
            }


            return Json(_pgKaryawan, JsonRequestBehavior.AllowGet);
        }



        public JsonResult UpdateKaryawan(FormCollection form)
        {
            ResponsesKaryawan _rsKaryawan = new ResponsesKaryawan();

            try
            {

                tbl_karyawan _tbl_karyawan = new tbl_karyawan()
                {
                    karyawan_id = Guid.Parse(form["karyawan_id"].ToString()),
                    karyawan_nama = form["karyawan_nama"].ToString(),
                    karyawan_alamat = form["karyawan_alamat"].ToString(),
                    karyawan_username = form["karyawan_username"].ToString(),
                    karyawan_phone = form["karyawan_phone"].ToString(),
                    karyawan_password = form["karyawan_password"].ToString(),

                };


                DB.Entry(_tbl_karyawan).State = EntityState.Modified;

                int result = DB.SaveChanges();

                if (result > 0)
                {

                    _rsKaryawan.Message = "Data Berhasil diupdate";
                    _rsKaryawan.Status = false;
                    _rsKaryawan.Data = DB.view_karyawan.Where(x => x.karyawan_id == _tbl_karyawan.karyawan_id).SingleOrDefault();

                }
            }
            catch (Exception ex)
            {

                _rsKaryawan.Message = ex.Message;
                _rsKaryawan.Status = false;
                _rsKaryawan.Data = null;
            }


            return Json(_rsKaryawan, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult InsertKaryawan(FormCollection form)
        {

            ResponsesKaryawan _rsKaryawan = new ResponsesKaryawan();


            try
            {
                tbl_karyawan _tblKaryawan = new tbl_karyawan()
                {
                    karyawan_id = Guid.NewGuid(),
                    karyawan_nama = form["karyawan_nama"].ToString(),
                    karyawan_alamat = form["karyawan_alamat"].ToString(),
                    karyawan_username = form["karyawan_username"].ToString(),
                    karyawan_phone = form["karyawan_phone"].ToString(),
                    karyawan_password = form["karyawan_password"].ToString(),
                };

                DB.tbl_karyawan.Add(_tblKaryawan);
                DB.SaveChanges();

                _rsKaryawan.Message = "Insert Successfully";
                _rsKaryawan.Status = true;
                _rsKaryawan.Data = DB.view_karyawan.Where(x => x.karyawan_id == _tblKaryawan.karyawan_id).SingleOrDefault();

            }
            catch (Exception ex)
            {
                _rsKaryawan.Message = ex.Message;
                _rsKaryawan.Status = false;
                _rsKaryawan.Data = null;

            }

            return Json(_rsKaryawan, JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetKaryawanById(FormCollection form)
        {
            ResponsesKaryawan _rsKaryawan = new ResponsesKaryawan();



            try
            {
                Guid karyawan_id = Guid.Parse(form["karyawan_id"].ToString());

                int rowCount = DB.view_karyawan.Where(x => x.karyawan_id == karyawan_id).Count();

                if (rowCount > 0)
                {
                    _rsKaryawan.Message = "Data ditemukan";
                    _rsKaryawan.Status = true;
                    _rsKaryawan.Data = DB.view_karyawan.Where(x => x.karyawan_id == karyawan_id).SingleOrDefault();

                }


            }
            catch (Exception ex)
            {

                _rsKaryawan.Message = ex.Message;
                _rsKaryawan.Status = false;
                _rsKaryawan.Data = null;
            }

            return Json(_rsKaryawan, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult DeleteKaryawan(FormCollection form)
        {
            ResponsesKaryawan _rsKaryawan = new ResponsesKaryawan();
            try
            {
                Guid karyawan_id = Guid.Parse(form["karyawan_id"].ToString());

                tbl_karyawan _tbl_karyawan = DB.tbl_karyawan.Find(karyawan_id);

                if (_tbl_karyawan != null)
                {
                    DB.tbl_karyawan.Remove(_tbl_karyawan);
                    int result = DB.SaveChanges();

                    if (result > 0)
                    {
                        _rsKaryawan.Message = "Berhasil menghapus data.";
                        _rsKaryawan.Status = true;
                        _rsKaryawan.Data = null;
                    }
                    else
                    {
                        _rsKaryawan.Message = "Gagal menghapus data";
                        _rsKaryawan.Status = false;
                        _rsKaryawan.Data = null;
                    }
                }
            }
            catch (Exception ex)
            {

                _rsKaryawan.Message = ex.Message;
                _rsKaryawan.Status = false;
                _rsKaryawan.Data = null;
            }

            return Json(_rsKaryawan, JsonRequestBehavior.AllowGet);
        }



        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                DB.Dispose();
            }
            base.Dispose(disposing);
        }


    }


}
