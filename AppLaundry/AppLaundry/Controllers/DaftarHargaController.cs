﻿using AppLaundry.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AppLaundry.Controllers
{
    public class DaftarHargaController : Controller
    {
        LaundryEntities DB = new LaundryEntities();

        // GET: Karyawan
        public ActionResult Index()
        {
            return View();
        }


        public JsonResult ListDaftarHarga()
        {

            PaginationDaftarHarga _pgDaftarHarga = new PaginationDaftarHarga();

            try
            {
                var draw = Request.Form.GetValues("draw").FirstOrDefault();
                var start = Request.Form.GetValues("start").FirstOrDefault();
                var length = Request.Form.GetValues("length").FirstOrDefault();
                var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();
                var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();


                // paging
                //Paging Size (10,20,50,100)    
                int pageSize = length != null ? Convert.ToInt32(length) : 0;

                int skip = start != null ? Convert.ToInt32(start) : 0;

                int recordsTotal = 0;


                // Getting all Customer data    
                var _daftar_harga = DB.view_daftar_harga.ToList();


                //Sorting    
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortDir)))
                {
                    //_karyawan = _karyawan.OrderBy(sortColumn + " " + sortDir);
                }

                //Search    
                if (!string.IsNullOrEmpty(searchValue))
                {
                    //_karyawan = _karyawan.Where(m => m.karyawan_alamat == searchValue);
                }


                //total number of rows count     
                recordsTotal = _daftar_harga.Count();

                var data = _daftar_harga.Skip(skip).Take(pageSize).ToList();

                _pgDaftarHarga.draw = Convert.ToInt32(draw);
                _pgDaftarHarga.recordsFiltered = recordsTotal;
                _pgDaftarHarga.recordsTotal = recordsTotal;
                _pgDaftarHarga.data = data;

            }
            catch (Exception ex)
            {

                string message = ex.Message;
            }


            return Json(_pgDaftarHarga, JsonRequestBehavior.AllowGet);
        }



        public JsonResult UpdateDaftarHarga(FormCollection form)
        {
            ResponsesDaftarHarga _pgDaftarHarga = new ResponsesDaftarHarga();

            try
            {

                tbl_daftar_harga _tbl_daftar_harga = new tbl_daftar_harga()
                {
                    daftar_harga_id = Guid.Parse(form["daftar_harga_id"].ToString()),
                    daftar_harga_satuan = form["daftar_harga_satuan"].ToString(),
                    daftar_harga_nama = form["daftar_harga_nama"].ToString(),
                    daftar_harga_harga = Convert.ToInt32(form["daftar_harga_harga"].ToString()),

                };


                DB.Entry(_tbl_daftar_harga).State = EntityState.Modified;

                int result = DB.SaveChanges();

                if (result > 0)
                {

                    _pgDaftarHarga.Message = "Berhasil mengubah data";
                    _pgDaftarHarga.Status = false;
                    _pgDaftarHarga.Data = DB.view_daftar_harga.Where(x => x.daftar_harga_id == _tbl_daftar_harga.daftar_harga_id).SingleOrDefault();

                }
            }
            catch (Exception ex)
            {

                _pgDaftarHarga.Message = ex.Message;
                _pgDaftarHarga.Status = false;
                _pgDaftarHarga.Data = null;
            }


            return Json(_pgDaftarHarga, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult InsertDaftarHarga(FormCollection form)
        {

            ResponsesDaftarHarga _pgDaftarHarga = new ResponsesDaftarHarga();


            try
            {
                tbl_daftar_harga _tbl_daftar_harga = new tbl_daftar_harga()
                {
                    daftar_harga_id = Guid.NewGuid(),
                    daftar_harga_satuan = form["daftar_harga_satuan"].ToString(),
                    daftar_harga_nama = form["daftar_harga_nama"].ToString(),
                    daftar_harga_harga = Convert.ToInt32(form["daftar_harga_harga"].ToString()),
                };

                DB.tbl_daftar_harga.Add(_tbl_daftar_harga);
                DB.SaveChanges();

                _pgDaftarHarga.Message = "Berhasil menyimpan data";
                _pgDaftarHarga.Status = true;
                _pgDaftarHarga.Data = DB.view_daftar_harga.Where(x => x.daftar_harga_id == _tbl_daftar_harga.daftar_harga_id).SingleOrDefault();

            }
            catch (Exception ex)
            {
                _pgDaftarHarga.Message = ex.Message;
                _pgDaftarHarga.Status = false;
                _pgDaftarHarga.Data = null;

            }

            return Json(_pgDaftarHarga, JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetDaftarHargaById(FormCollection form)
        {
            ResponsesDaftarHarga _pgDaftarHarga = new ResponsesDaftarHarga();



            try
            {
                Guid daftar_harga_id = Guid.Parse(form["daftar_harga_id"].ToString());

                int rowCount = DB.view_daftar_harga.Where(x => x.daftar_harga_id == daftar_harga_id).Count();

                if (rowCount > 0)
                {
                    _pgDaftarHarga.Message = "Data ditemukan";
                    _pgDaftarHarga.Status = true;
                    _pgDaftarHarga.Data = DB.view_daftar_harga.Where(x => x.daftar_harga_id == daftar_harga_id).SingleOrDefault();

                }


            }
            catch (Exception ex)
            {

                _pgDaftarHarga.Message = ex.Message;
                _pgDaftarHarga.Status = false;
                _pgDaftarHarga.Data = null;
            }

            return Json(_pgDaftarHarga, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult DeleteDaftarHarga(FormCollection form)
        {
            ResponsesDaftarHarga _pgDaftarHarga = new ResponsesDaftarHarga();
            try
            {
                Guid daftar_harga_id = Guid.Parse(form["daftar_harga_id"].ToString());

                tbl_daftar_harga _tbl_daftar_harga = DB.tbl_daftar_harga.Find(daftar_harga_id);

                if (_tbl_daftar_harga != null)
                {
                    DB.tbl_daftar_harga.Remove(_tbl_daftar_harga);
                    int result = DB.SaveChanges();

                    if (result > 0)
                    {
                        _pgDaftarHarga.Message = "Berhasil menghapus data.";
                        _pgDaftarHarga.Status = true;
                        _pgDaftarHarga.Data = null;
                    }
                    else
                    {
                        _pgDaftarHarga.Message = "Gagal menghapus data.";
                        _pgDaftarHarga.Status = false;
                        _pgDaftarHarga.Data = null;
                    }
                }
            }
            catch (Exception ex)
            {

                _pgDaftarHarga.Message = ex.Message;
                _pgDaftarHarga.Status = false;
                _pgDaftarHarga.Data = null;
            }

            return Json(_pgDaftarHarga, JsonRequestBehavior.AllowGet);
        }



        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                DB.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}