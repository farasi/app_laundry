USE [db_laundry]
GO
EXEC sys.sp_dropextendedproperty @name=N'MS_DiagramPaneCount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'view_transaksi_detail'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_DiagramPane1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'view_transaksi_detail'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_DiagramPaneCount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'view_transaksi'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_DiagramPane1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'view_transaksi'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_DiagramPaneCount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'view_pelanggan'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_DiagramPane1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'view_pelanggan'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_DiagramPaneCount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'view_karyawan'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_DiagramPane1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'view_karyawan'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_DiagramPaneCount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'view_daftar_harga'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_DiagramPane1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'view_daftar_harga'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbl_transaksi', @level2type=N'COLUMN',@level2name=N'transaksi_status'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbl_detail_transaksi', @level2type=N'COLUMN',@level2name=N'detail_transaksi_status'

GO
ALTER TABLE [dbo].[tbl_transaksi] DROP CONSTRAINT [FK_tbl_transaksi_tbl_transaksi]
GO
ALTER TABLE [dbo].[tbl_detail_transaksi] DROP CONSTRAINT [FK_tbl_detail_transaksi_tbl_transaksi]
GO
ALTER TABLE [dbo].[tbl_detail_transaksi] DROP CONSTRAINT [FK_tbl_detail_transaksi_tbl_daftar_harga]
GO
ALTER TABLE [dbo].[tbl_transaksi] DROP CONSTRAINT [DF_tbl_transaksi_transaksi_status]
GO
ALTER TABLE [dbo].[tbl_tipe] DROP CONSTRAINT [DF_tbl_tipe_tipe_deleted]
GO
ALTER TABLE [dbo].[tbl_pelanggan] DROP CONSTRAINT [DF_tbl_pelanggan_pelanggan_deleted]
GO
ALTER TABLE [dbo].[tbl_detail_transaksi] DROP CONSTRAINT [DF_tbl_detail_transaksi_detail_transaksi_status]
GO
ALTER TABLE [dbo].[tbl_daftar_harga] DROP CONSTRAINT [DF_tbl_daftar_harga_daftar_harga_deleted]
GO
/****** Object:  View [dbo].[view_transaksi_detail]    Script Date: 24/07/2019 19:43:03 ******/
DROP VIEW [dbo].[view_transaksi_detail]
GO
/****** Object:  View [dbo].[view_transaksi]    Script Date: 24/07/2019 19:43:03 ******/
DROP VIEW [dbo].[view_transaksi]
GO
/****** Object:  View [dbo].[view_pelanggan]    Script Date: 24/07/2019 19:43:03 ******/
DROP VIEW [dbo].[view_pelanggan]
GO
/****** Object:  View [dbo].[view_karyawan]    Script Date: 24/07/2019 19:43:03 ******/
DROP VIEW [dbo].[view_karyawan]
GO
/****** Object:  View [dbo].[view_daftar_harga]    Script Date: 24/07/2019 19:43:03 ******/
DROP VIEW [dbo].[view_daftar_harga]
GO
/****** Object:  Table [dbo].[tbl_transaksi]    Script Date: 24/07/2019 19:43:03 ******/
DROP TABLE [dbo].[tbl_transaksi]
GO
/****** Object:  Table [dbo].[tbl_tipe]    Script Date: 24/07/2019 19:43:03 ******/
DROP TABLE [dbo].[tbl_tipe]
GO
/****** Object:  Table [dbo].[tbl_product]    Script Date: 24/07/2019 19:43:03 ******/
DROP TABLE [dbo].[tbl_product]
GO
/****** Object:  Table [dbo].[tbl_pelanggan]    Script Date: 24/07/2019 19:43:03 ******/
DROP TABLE [dbo].[tbl_pelanggan]
GO
/****** Object:  Table [dbo].[tbl_karyawan]    Script Date: 24/07/2019 19:43:03 ******/
DROP TABLE [dbo].[tbl_karyawan]
GO
/****** Object:  Table [dbo].[tbl_detail_transaksi]    Script Date: 24/07/2019 19:43:03 ******/
DROP TABLE [dbo].[tbl_detail_transaksi]
GO
/****** Object:  Table [dbo].[tbl_daftar_harga]    Script Date: 24/07/2019 19:43:03 ******/
DROP TABLE [dbo].[tbl_daftar_harga]
GO
/****** Object:  StoredProcedure [dbo].[sp_temp_detail_transaksi]    Script Date: 24/07/2019 19:43:03 ******/
DROP PROCEDURE [dbo].[sp_temp_detail_transaksi]
GO
/****** Object:  StoredProcedure [dbo].[sp_temp_detail_transaksi]    Script Date: 24/07/2019 19:43:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ebid
-- Create date: 11/07/2019
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[sp_temp_detail_transaksi] 
	-- Add the parameters for the stored procedure 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	CREATE TABLE #temp_detail_transaksi (
		temp_detail_transaksi_id uniqueidentifier,
		temp_detail_transaksi_transaksi_id uniqueidentifier,
		temp_detail_transaksi_daftar_harga_id uniqueidentifier,
		temp_detail_transaksi_keterangan varchar(50),
		temp_detail_transaksi_kuantitas Int,
		temp_detail_transaksi_satuan varchar(8),
		temp_detail_transaksi_harga_satuan int,
		temp_detail_transaksi_harga_subtotal int,

	);
END


GO
/****** Object:  Table [dbo].[tbl_daftar_harga]    Script Date: 24/07/2019 19:43:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_daftar_harga](
	[daftar_harga_id] [uniqueidentifier] NOT NULL,
	[daftar_harga_satuan] [varchar](8) NULL,
	[daftar_harga_nama] [varchar](32) NULL,
	[daftar_harga_harga] [int] NULL,
	[daftar_harga_created_datetime] [datetime] NULL,
	[daftar_harga_modified_datetime] [datetime] NULL,
	[daftar_harga_deleted] [bit] NULL,
 CONSTRAINT [PK_tbl_daftar_harga] PRIMARY KEY CLUSTERED 
(
	[daftar_harga_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_detail_transaksi]    Script Date: 24/07/2019 19:43:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_detail_transaksi](
	[detail_transaksi_id] [uniqueidentifier] NOT NULL,
	[detail_transaksi_transaksi_id] [uniqueidentifier] NULL,
	[detail_transaksi_daftar_harga_id] [uniqueidentifier] NULL,
	[detail_transaksi_keterangan] [varchar](50) NULL,
	[detail_transaksi_kuantitas] [int] NULL,
	[detail_transaksi_harga_satuan] [int] NULL,
	[detail_transaksi_harga_subtotal] [int] NULL,
	[detail_transaksi_no_invoice] [varchar](10) NULL,
	[detail_transaksi_status] [varchar](8) NULL,
 CONSTRAINT [PK_tbl_detail_transaksi] PRIMARY KEY CLUSTERED 
(
	[detail_transaksi_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_karyawan]    Script Date: 24/07/2019 19:43:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_karyawan](
	[karyawan_id] [uniqueidentifier] NOT NULL,
	[karyawan_nama] [varchar](50) NULL,
	[karyawan_alamat] [text] NULL,
	[karyawan_phone] [varchar](16) NULL,
	[karyawan_username] [varchar](8) NULL,
	[karyawan_password] [varchar](256) NULL,
	[karyawan_created_datetime] [datetime] NULL,
	[karyawan_modified_datetime] [datetime] NULL,
	[karyawan_deleted] [bit] NULL,
 CONSTRAINT [PK_tbl_karyawan] PRIMARY KEY CLUSTERED 
(
	[karyawan_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_pelanggan]    Script Date: 24/07/2019 19:43:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_pelanggan](
	[pelanggan_id] [uniqueidentifier] NOT NULL,
	[pelanggan_nama] [varchar](50) NULL,
	[pelanggan_alamat] [text] NULL,
	[pelanggan_phone] [varchar](16) NULL,
	[pelanggan_created_datetime] [datetime] NULL,
	[pelanggan_modified_datetime] [datetime] NULL,
	[pelanggan_deleted] [bit] NULL,
 CONSTRAINT [PK_tbl_pelanggan] PRIMARY KEY CLUSTERED 
(
	[pelanggan_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_product]    Script Date: 24/07/2019 19:43:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_product](
	[product_nama] [varchar](20) NULL,
	[product_kuantitas] [decimal](18, 2) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_tipe]    Script Date: 24/07/2019 19:43:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_tipe](
	[tipe_id] [uniqueidentifier] NOT NULL,
	[tipe_daftar_harga_id] [uniqueidentifier] NULL,
	[tipe_created_datetime] [datetime] NULL,
	[tipe_modified_datetime] [datetime] NULL,
	[tipe_deleted] [bit] NULL,
 CONSTRAINT [PK_tbl_tipe] PRIMARY KEY CLUSTERED 
(
	[tipe_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_transaksi]    Script Date: 24/07/2019 19:43:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_transaksi](
	[transaksi_id] [uniqueidentifier] NOT NULL,
	[transaksi_pelanggan_id] [uniqueidentifier] NULL,
	[transaksi_datetime] [datetime] NULL,
	[transaksi_created_datetime] [datetime] NULL,
	[transaksi_modified_datetime] [datetime] NULL,
	[transaksi_status] [varchar](8) NULL,
	[transaksi_created_karyawan_id] [uniqueidentifier] NULL,
	[transaksi_modified_karyawan_id] [uniqueidentifier] NULL,
	[transaksi_total] [decimal](18, 2) NULL,
 CONSTRAINT [PK_tbl_transaksi] PRIMARY KEY CLUSTERED 
(
	[transaksi_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  View [dbo].[view_daftar_harga]    Script Date: 24/07/2019 19:43:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_daftar_harga]
AS
SELECT        daftar_harga_id, daftar_harga_satuan, daftar_harga_nama, daftar_harga_harga, daftar_harga_created_datetime, daftar_harga_modified_datetime, daftar_harga_deleted
FROM            dbo.tbl_daftar_harga


GO
/****** Object:  View [dbo].[view_karyawan]    Script Date: 24/07/2019 19:43:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_karyawan]
AS
SELECT        karyawan_id, karyawan_nama, karyawan_alamat, karyawan_phone, karyawan_username, karyawan_password, karyawan_created_datetime, karyawan_modified_datetime, karyawan_deleted
FROM            dbo.tbl_karyawan


GO
/****** Object:  View [dbo].[view_pelanggan]    Script Date: 24/07/2019 19:43:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_pelanggan]
AS
SELECT        pelanggan_id, pelanggan_nama, pelanggan_alamat, pelanggan_phone, pelanggan_created_datetime, pelanggan_modified_datetime, pelanggan_deleted
FROM            dbo.tbl_pelanggan


GO
/****** Object:  View [dbo].[view_transaksi]    Script Date: 24/07/2019 19:43:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_transaksi]
AS
SELECT dbo.tbl_transaksi.transaksi_id, dbo.tbl_transaksi.transaksi_pelanggan_id, dbo.tbl_transaksi.transaksi_datetime, dbo.tbl_transaksi.transaksi_created_datetime, dbo.tbl_transaksi.transaksi_modified_datetime, 
                  dbo.tbl_transaksi.transaksi_status, dbo.tbl_transaksi.transaksi_created_karyawan_id, dbo.tbl_transaksi.transaksi_modified_karyawan_id, dbo.tbl_transaksi.transaksi_total, dbo.tbl_pelanggan.pelanggan_nama, 
                  dbo.tbl_pelanggan.pelanggan_alamat, dbo.tbl_pelanggan.pelanggan_phone, dbo.tbl_pelanggan.pelanggan_created_datetime, dbo.tbl_pelanggan.pelanggan_modified_datetime, dbo.tbl_pelanggan.pelanggan_deleted, 
                  dbo.tbl_pelanggan.pelanggan_id
FROM     dbo.tbl_transaksi INNER JOIN
                  dbo.tbl_pelanggan ON dbo.tbl_transaksi.transaksi_pelanggan_id = dbo.tbl_pelanggan.pelanggan_id

GO
/****** Object:  View [dbo].[view_transaksi_detail]    Script Date: 24/07/2019 19:43:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_transaksi_detail]
AS
SELECT        detail_transaksi_id, detail_transaksi_transaksi_id, detail_transaksi_daftar_harga_id, detail_transaksi_keterangan, detail_transaksi_kuantitas, detail_transaksi_harga_satuan, detail_transaksi_harga_subtotal, 
                         detail_transaksi_no_invoice, detail_transaksi_status
FROM            dbo.tbl_detail_transaksi


GO
INSERT [dbo].[tbl_daftar_harga] ([daftar_harga_id], [daftar_harga_satuan], [daftar_harga_nama], [daftar_harga_harga], [daftar_harga_created_datetime], [daftar_harga_modified_datetime], [daftar_harga_deleted]) VALUES (N'd9a6f4b4-c476-4290-9103-096bd6a1bfa1', N'PCS', N'Karpet', 20000, NULL, NULL, NULL)
GO
INSERT [dbo].[tbl_daftar_harga] ([daftar_harga_id], [daftar_harga_satuan], [daftar_harga_nama], [daftar_harga_harga], [daftar_harga_created_datetime], [daftar_harga_modified_datetime], [daftar_harga_deleted]) VALUES (N'cd1b7a3d-9fd5-4aa6-8af3-203f57b7cba5', N'PCS', N'Mejas', 10000, NULL, NULL, NULL)
GO
INSERT [dbo].[tbl_daftar_harga] ([daftar_harga_id], [daftar_harga_satuan], [daftar_harga_nama], [daftar_harga_harga], [daftar_harga_created_datetime], [daftar_harga_modified_datetime], [daftar_harga_deleted]) VALUES (N'b0fbd914-59d6-4739-a7af-34d5e1570d96', N'Kilogram', N'KG', 7000, CAST(0x0000AA87011077BB AS DateTime), CAST(0x0000AA87011077BB AS DateTime), 0)
GO
INSERT [dbo].[tbl_daftar_harga] ([daftar_harga_id], [daftar_harga_satuan], [daftar_harga_nama], [daftar_harga_harga], [daftar_harga_created_datetime], [daftar_harga_modified_datetime], [daftar_harga_deleted]) VALUES (N'f67ea350-962c-424f-a3af-636e9cbe3189', N'PCS', N'Lemari', 20000, NULL, NULL, NULL)
GO
INSERT [dbo].[tbl_daftar_harga] ([daftar_harga_id], [daftar_harga_satuan], [daftar_harga_nama], [daftar_harga_harga], [daftar_harga_created_datetime], [daftar_harga_modified_datetime], [daftar_harga_deleted]) VALUES (N'8c4c680a-f0ef-4e28-9a31-b0b06ecb09d6', N'PCS', N'Tas', 9000, CAST(0x0000AA87011F497C AS DateTime), CAST(0x0000AA87011F497C AS DateTime), 0)
GO
INSERT [dbo].[tbl_karyawan] ([karyawan_id], [karyawan_nama], [karyawan_alamat], [karyawan_phone], [karyawan_username], [karyawan_password], [karyawan_created_datetime], [karyawan_modified_datetime], [karyawan_deleted]) VALUES (N'aa96bcd9-eb95-4fc3-9f41-0be27ae0d8ed', N'Ebid', N'Mediterania 2 Jakarta Barat', N'089677102572', N'ebidboy', N'ebidboy', NULL, NULL, NULL)
GO
INSERT [dbo].[tbl_pelanggan] ([pelanggan_id], [pelanggan_nama], [pelanggan_alamat], [pelanggan_phone], [pelanggan_created_datetime], [pelanggan_modified_datetime], [pelanggan_deleted]) VALUES (N'fce8d4fb-902a-4c27-b240-4a262fad675a', N'Bobby', N'Villa Indah Ciomas Blok M5 No.6', N'089677101572', NULL, NULL, NULL)
GO
INSERT [dbo].[tbl_product] ([product_nama], [product_kuantitas]) VALUES (N'mie', CAST(22.00 AS Decimal(18, 2)))
GO
INSERT [dbo].[tbl_product] ([product_nama], [product_kuantitas]) VALUES (N'mie-soto', CAST(4.00 AS Decimal(18, 2)))
GO
INSERT [dbo].[tbl_product] ([product_nama], [product_kuantitas]) VALUES (N'pop-mie', CAST(5.00 AS Decimal(18, 2)))
GO
INSERT [dbo].[tbl_product] ([product_nama], [product_kuantitas]) VALUES (N'mie', CAST(22.00 AS Decimal(18, 2)))
GO
INSERT [dbo].[tbl_product] ([product_nama], [product_kuantitas]) VALUES (N'mie-soto', CAST(4.00 AS Decimal(18, 2)))
GO
INSERT [dbo].[tbl_product] ([product_nama], [product_kuantitas]) VALUES (N'pop-mie', CAST(5.00 AS Decimal(18, 2)))
GO
ALTER TABLE [dbo].[tbl_daftar_harga] ADD  CONSTRAINT [DF_tbl_daftar_harga_daftar_harga_deleted]  DEFAULT ((0)) FOR [daftar_harga_deleted]
GO
ALTER TABLE [dbo].[tbl_detail_transaksi] ADD  CONSTRAINT [DF_tbl_detail_transaksi_detail_transaksi_status]  DEFAULT ('standby') FOR [detail_transaksi_status]
GO
ALTER TABLE [dbo].[tbl_pelanggan] ADD  CONSTRAINT [DF_tbl_pelanggan_pelanggan_deleted]  DEFAULT ((0)) FOR [pelanggan_deleted]
GO
ALTER TABLE [dbo].[tbl_tipe] ADD  CONSTRAINT [DF_tbl_tipe_tipe_deleted]  DEFAULT ((0)) FOR [tipe_deleted]
GO
ALTER TABLE [dbo].[tbl_transaksi] ADD  CONSTRAINT [DF_tbl_transaksi_transaksi_status]  DEFAULT ('standby') FOR [transaksi_status]
GO
ALTER TABLE [dbo].[tbl_detail_transaksi]  WITH CHECK ADD  CONSTRAINT [FK_tbl_detail_transaksi_tbl_daftar_harga] FOREIGN KEY([detail_transaksi_daftar_harga_id])
REFERENCES [dbo].[tbl_daftar_harga] ([daftar_harga_id])
GO
ALTER TABLE [dbo].[tbl_detail_transaksi] CHECK CONSTRAINT [FK_tbl_detail_transaksi_tbl_daftar_harga]
GO
ALTER TABLE [dbo].[tbl_detail_transaksi]  WITH CHECK ADD  CONSTRAINT [FK_tbl_detail_transaksi_tbl_transaksi] FOREIGN KEY([detail_transaksi_transaksi_id])
REFERENCES [dbo].[tbl_transaksi] ([transaksi_id])
GO
ALTER TABLE [dbo].[tbl_detail_transaksi] CHECK CONSTRAINT [FK_tbl_detail_transaksi_tbl_transaksi]
GO
ALTER TABLE [dbo].[tbl_transaksi]  WITH CHECK ADD  CONSTRAINT [FK_tbl_transaksi_tbl_transaksi] FOREIGN KEY([transaksi_pelanggan_id])
REFERENCES [dbo].[tbl_pelanggan] ([pelanggan_id])
GO
ALTER TABLE [dbo].[tbl_transaksi] CHECK CONSTRAINT [FK_tbl_transaksi_tbl_transaksi]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'standby, proses, ready, finish' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbl_detail_transaksi', @level2type=N'COLUMN',@level2name=N'detail_transaksi_status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'standby, proses, ready, finish' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbl_transaksi', @level2type=N'COLUMN',@level2name=N'transaksi_status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "tbl_daftar_harga"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 298
            End
            DisplayFlags = 280
            TopColumn = 3
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'view_daftar_harga'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'view_daftar_harga'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "tbl_karyawan"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 282
            End
            DisplayFlags = 280
            TopColumn = 5
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'view_karyawan'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'view_karyawan'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "tbl_pelanggan"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 288
            End
            DisplayFlags = 280
            TopColumn = 3
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'view_pelanggan'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'view_pelanggan'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "tbl_transaksi"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 296
            End
            DisplayFlags = 280
            TopColumn = 3
         End
         Begin Table = "tbl_pelanggan"
            Begin Extent = 
               Top = 7
               Left = 344
               Bottom = 170
               Right = 643
            End
            DisplayFlags = 280
            TopColumn = 3
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1176
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1356
         SortOrder = 1416
         GroupBy = 1350
         Filter = 1356
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'view_transaksi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'view_transaksi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "tbl_detail_transaksi"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 293
            End
            DisplayFlags = 280
            TopColumn = 5
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'view_transaksi_detail'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'view_transaksi_detail'
GO
